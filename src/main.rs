use handle_errors::return_error;
use warp::Filter;

mod routes;
mod store;
mod types;

use crate::routes::{
    answer::add_answer,
    question::{add_question, delete_question, get_questions, update_question},
};
use crate::store::Store;

#[tokio::main]
async fn main() {
    let store = Store::new();
    let store_filter = warp::any().map(move || store.clone());

    let get_items = warp::get()
        .and(warp::path("questions"))
        .and(warp::path::end())
        .and(warp::query())
        .and(store_filter.clone())
        .and_then(get_questions);

    let add_item = warp::post()
        .and(warp::path("questions"))
        .and(warp::path::end())
        .and(store_filter.clone())
        .and(warp::body::json())
        .and_then(add_question);

    let update_item = warp::put()
        .and(warp::path("questions"))
        .and(warp::path::param::<String>())
        .and(warp::path::end())
        .and(store_filter.clone())
        .and(warp::body::json())
        .and_then(update_question);

    let delete_item = warp::delete()
        .and(warp::path("questions"))
        .and(warp::path::param::<String>())
        .and(warp::path::end())
        .and(store_filter.clone())
        .and_then(delete_question);

    // TODO: use /questions/:questionId/answers instead?
    let add_answer = warp::post()
        .and(warp::path("answers"))
        .and(warp::path::end())
        .and(store_filter.clone())
        .and(warp::body::form())
        .and_then(add_answer);

    let routes = get_items
        .or(add_item)
        .or(update_item)
        .or(delete_item)
        .or(add_answer)
        .recover(return_error);

    warp::serve(routes).run(([127, 0, 0, 1], 4000)).await;
}
