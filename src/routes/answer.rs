use crate::store::Store;
use crate::types::{
    answer::{Answer, AnswerId},
    question::QuestionId,
};
use std::collections::HashMap;
use warp::http::StatusCode;

pub async fn add_answer(
    store: Store,
    params: HashMap<String, String>,
) -> Result<impl warp::Reply, warp::Rejection> {
    let answer = Answer {
        // TODO: hard coded ID
        id: AnswerId("1".to_string()),
        // TODO: unwrap
        content: params.get("content").unwrap().to_string(),
        // TODO: unwrap
        // TODO: does the question exist?
        question_id: QuestionId(params.get("questionId").unwrap().to_string()),
    };

    store
        .answers
        .write()
        .await
        .insert(answer.id.clone(), answer);

    Ok(warp::reply::with_status("Answer added", StatusCode::OK))
}
